<?php
require("db.php");

// Get parameters from URL (example: genxml.php?lat=49&lng=8.6&radius=200&specialshop=bulls)
// specialshop is optional
$center_lat = $_GET["lat"];
$center_lng = $_GET["lng"];
$radius = $_GET["radius"];
$specialshop = $_GET["specialshop"];
$address = $_GET["address"];

// Start XML file, create parent node
$dom = new DOMDocument('1.0', 'UTF-8');
$node = $dom->createElement("markers");
$parnode = $dom->appendChild($node);
// we want a nice output
$dom->formatOutput = true;
/*
// Opens a connection to a mySQL server
$connection=mysql_connect ($ipaddress, $username, $password);
if (!$connection) {
  die("Not connected : " . mysql_error());
}

// Set the active mySQL database
$db_selected = mysql_select_db($database, $connection);
if (!$db_selected) {
  die ("Can\'t use db : " . mysql_error());
}

// Search the rows in the markers table
if ($specialshop == "bulls") {
	$query = sprintf("SELECT NameW, Name1, Name2, Name3, AdrNr, PLZ, ORT, Strasse, Telefon, Telefax, EMail, Url, Motorfahrzeuge, Heimtrainer, EBike, ORC, DahonKC, Entfernung, isBullsShop, isPegasusShop, isKTMShop, isMaybeKTMShop, Lat, Lng, ( 6371 * acos( cos( radians('%s') ) * cos( radians( Lat ) ) * cos( radians( Lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( Lat ) ) ) ) AS distance FROM stores WHERE isBullsShop = 1 HAVING distance < '%s' ORDER BY distance",
					mysql_real_escape_string($center_lat),
				 	mysql_real_escape_string($center_lng),
				 	mysql_real_escape_string($center_lat),
				 	mysql_real_escape_string($radius));
} else if ($specialshop == "pegasus") {
	$query = sprintf("SELECT NameW, Name1, Name2, Name3, AdrNr, PLZ, ORT, Strasse, Telefon, Telefax, EMail, Url, Motorfahrzeuge, Heimtrainer, EBike, ORC, DahonKC, Entfernung, isBullsShop, isPegasusShop, isKTMShop, isMaybeKTMShop, Lat, Lng, ( 6371 * acos( cos( radians('%s') ) * cos( radians( Lat ) ) * cos( radians( Lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( Lat ) ) ) ) AS distance FROM stores WHERE isPegasusShop = 1 HAVING distance < '%s' ORDER BY distance",
					mysql_real_escape_string($center_lat),
				 	mysql_real_escape_string($center_lng),
				 	mysql_real_escape_string($center_lat),
				 	mysql_real_escape_string($radius));
} else if ($specialshop == "ktm") {
	$query = sprintf("SELECT NameW, Name1, Name2, Name3, AdrNr, PLZ, ORT, Strasse, Telefon, Telefax, EMail, Url, Motorfahrzeuge, Heimtrainer, EBike, ORC, DahonKC, Entfernung, isBullsShop, isPegasusShop, isKTMShop, isMaybeKTMShop, Lat, Lng, ( 6371 * acos( cos( radians('%s') ) * cos( radians( Lat ) ) * cos( radians( Lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( Lat ) ) ) ) AS distance FROM stores WHERE isKTMShop = 1 HAVING distance < '%s' ORDER BY distance",
					mysql_real_escape_string($center_lat),
				 	mysql_real_escape_string($center_lng),
				 	mysql_real_escape_string($center_lat),
				 	mysql_real_escape_string($radius));
} else {
	$query = sprintf("SELECT NameW, Name1, Name2, Name3, AdrNr, PLZ, ORT, Strasse, Telefon, Telefax, EMail, Url, Motorfahrzeuge, Heimtrainer, EBike, ORC, DahonKC, Entfernung, isBullsShop, isPegasusShop, isKTMShop, isMaybeKTMShop, Lat, Lng, ( 6371 * acos( cos( radians('%s') ) * cos( radians( Lat ) ) * cos( radians( Lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( Lat ) ) ) ) AS distance FROM stores HAVING distance < '%s' ORDER BY distance",
					mysql_real_escape_string($center_lat),
				 	mysql_real_escape_string($center_lng),
				 	mysql_real_escape_string($center_lat),
				 	mysql_real_escape_string($radius));
}
$result = mysql_query($query);

if (!$result) {
  die("Invalid query: " . mysql_error());
}
*/


// Iterate through the rows, adding XML nodes for each.
// iconv is used to convert from ISO-8859-1 to UTF-8.
// Without this conversion setAttribute would cause errors when reading Umlauts.


$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
// führe die Aktion aus und gebe die Daten an den Browser weiter

$plzorort = (is_numeric(urlencode($address)) ? "PLZ" : "Ort");
curl_setopt($ch, CURLOPT_URL, 'http://haendlersuche.zeg.de/haendlersuche.aspx?Lkz=D&' . $plzorort . '=' . urlencode($address));

//old 24.02.2014: curl_setopt($ch, CURLOPT_URL, 'http://haendlersuche.zeg.de/haendlersuche.aspx?Lkz=D&Ort='.$address.'&Webseite='.$specialshop);

//$api = simplexml_load_string('http://haendlersuche.zeg.de/haendlersuche.aspx?Lkz=D&Ort='.$address.'&Webseite=bikektmzeg');
//$xml = file_get_contents('http://haendlersuche.zeg.de/haendlersuche.aspx?Lkz=D&Ort='.$address.'&Webseite=bikektmzeg');
//$api = simplexml_load_string($xml);
/*
$api = simplexml_load_string(
    utf8_encode(
        curl_exec($ch)
    )
);
*/

$api = simplexml_load_string(

    str_replace("utf-16", "utf-8", curl_exec($ch))

);

//$result = $api->NewDataSet;
$result = $api;
/* 		print_r($result);

       $info = array();

       // General information
       $info['city'] = (string)$result->forecast_information->city->attributes()->data;
       $info['date'] = (string)$result->forecast_information->forecast_date->attributes()->data;
       $info['time'] = (string)$result->forecast_information->current_date_time->attributes()->data;

       // Current weather
       $info[0]['condition'] = (string)$result->current_conditions->condition->attributes()->data;
*/
foreach ($result as $res) {
    if ((int)$res->Entfernung <= $radius) {
        $node = $dom->createElement("marker");
        $newnode = $parnode->appendChild($node);
        $newnode->setAttribute("street", (string)$res->Strasse);
        $newnode->setAttribute("postalcode", (string)$res->PLZ);
        $newnode->setAttribute("city", (string)$res->ORT);
        $newnode->setAttribute("name", (string)$res->NameW);
        $newnode->setAttribute("mail", (string)$res->EMail);
        $newnode->setAttribute("tel", (string)$res->Telefon);
        $newnode->setAttribute("fax", (string)$res->Telefax);
        $newnode->setAttribute("www", (string)$res->Url);
        $newnode->setAttribute("name", (string)$res->NameW);
        $newnode->setAttribute("lat", (string)$res->Latitude);
        $newnode->setAttribute("lng", (string)$res->Longitude);
        $newnode->setAttribute("distance", (string)$res->Entfernung);
    }
}
/*
while ($row = @mysql_fetch_assoc($result)) {
  $node = $dom->createElement("marker");
  $newnode = $parnode->appendChild($node);
  $newnode->setAttribute("street", iconv("ISO-8859-1", "UTF-8", $row['Strasse']));
  $newnode->setAttribute("postalcode", iconv("ISO-8859-1", "UTF-8", $row['PLZ']));
  $newnode->setAttribute("city", iconv("ISO-8859-1", "UTF-8", $row['ORT']));
  $newnode->setAttribute("name", iconv("ISO-8859-1", "UTF-8", $row['NameW']));
    $newnode->setAttribute("mail", iconv("ISO-8859-1", "UTF-8", $row['EMail']));
	  $newnode->setAttribute("tel", iconv("ISO-8859-1", "UTF-8", $row['Telefon']));
	  	  $newnode->setAttribute("fax", iconv("ISO-8859-1", "UTF-8", $row['Telefax']));
	    $newnode->setAttribute("www", iconv("ISO-8859-1", "UTF-8", $row['Url']));
  $newnode->setAttribute("name", iconv("ISO-8859-1", "UTF-8", $row['NameW']));
  $newnode->setAttribute("lat", iconv("ISO-8859-1", "UTF-8", $row['Lat']));
  $newnode->setAttribute("lng", iconv("ISO-8859-1", "UTF-8", $row['Lng']));
  $newnode->setAttribute("distance", iconv("ISO-8859-1", "UTF-8", $row['distance']));
}
*/
// Output
header("Content-type: text/xml");
echo $dom->saveXML();

?>