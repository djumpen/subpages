<?php

define('DIRSEP', DIRECTORY_SEPARATOR);

include('include/header.html');
if (isset($_GET['page']) && file_exists(__DIR__ . DIRSEP . $_GET['page'] . '.html')) {
    include(__DIR__ . DIRSEP . $_GET['page'] . '.html');
} else {
    include(__DIR__ . DIRSEP . 'technikal-data' . '.html');
}
include('include/footer.html');